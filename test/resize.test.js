const assert = require('assert');
const sinon = require('sinon')
const resize = require('../modules/resize.js');

describe('Resize image from path to the given size:', function () {
	it('should return the resized image', function () {
		beforeEach(function () {
			mock = sinon.mock('sharp');
		});

        let result = resize('../image/img_012343.jpg','jpg', 200, 150);
        
        assert.equal(result.options.width, 200);
        assert.equal(result.options.height, 150);
        assert.equal(result.options.formatOut, 'jpeg');
    });
});