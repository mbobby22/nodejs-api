const expect = require('chai').expect;
const resolution = require('../modules/resolution.js');

describe('Get image size from request:', function () {
    it('should return the first value as width', function () {
        let result = resolution.width('300x400');
        expect(result).to.equal('300');
    });

    it('should not return the first value as width', function () {
        let result = resolution.width('300-400');
        expect(result).to.equal(0);
    });

    it('should return the second value as height', function () {
        let result = resolution.height('200x150');
        expect(result).to.equal('150');
    });

    it('should not return the second value as height', function () {
        let result = resolution.height('200+150');
        expect(result).to.equal(0);
    });
});