const express = require('express');
const url = require('url');
const fs = require('fs');
const resize = require('./modules/resize');
const resolution = require('./modules/resolution');
const parameters = require('./config/parameters');

const server = express();

var swStats = require('swagger-stats');
server.use(swStats.getMiddleware({}));

server.get('/image/*', (req, res) => {
    const parsedUrl = url.parse(req.url, true);
    const localPath = __dirname + parsedUrl.pathname;

    if (!fs.existsSync(localPath)) {
        res.sendStatus(404);
    }

    if (parsedUrl.query.size) {
        var widthString = resolution.width(parsedUrl.query.size);
        var heightString = resolution.height(parsedUrl.query.size);
        var format = 'jpg'; // TODO can also be sent via query, harcoded it for now
    }

    // Parse to integer if possible
    let width, height;

    if (widthString) {
        width = parseInt(widthString);
    }

    if (heightString) {
        height = parseInt(heightString);
    }
    // Set the content-type of the response
    res.type(`image/${format || 'png'}`);

	// Get the resized image
    try {
	   resize(localPath, format, width, height).pipe(res);
    } catch (err) {
        res.sendStatus(404);
    }
});

server.get('/stats', (req, res) => {
    const cache = require('./modules/cache.js');

    promise = cache.displayStats();
    var stats = '';

    promise.then(function(value) {
        stats = value;
        res.write(stats);
        res.end();
    });
});

server.listen(parameters.server_port, () => {
});