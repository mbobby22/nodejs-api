# Node.js API

## Project scope

- image resize service
- uses a static set of image files, stored in a local folder: `/image`
- only accepts `.jpg` format currently
- it will receive `GET` request including image url and size (width x height)
- support requests for various resolutions
- e.g. usage: `/image/img_012343.jpg?size=300x400` will resize the original image at `300x400` and return it to the client

## Installation instructions

### Local:

1. install npm
2. clone project locally
3. install project
- run command 'npm install'
4. run image service with command 'npm start'
- this will start the server on localhost and port 8000
- you should see link working in browser

### Docker:

`docker-compose up -d`

Via Dockerfile:

1. navigate to app folder from terminal
2. run `docker build -t nodejs-api .` to build image
3. run `docker run -p 8000:8000 -d nodejs-api` to launch container
4. service will be available using link: http://localhost:8000/image/img_012343.jpg?size=300x400

### How to

- open a browser and navigate to 'http://localhost:8000/image/img_012343.jpg?size=300x400'
- you can also use Postman or other similar client
- you should see the image with the size width=300 and height=400
- you can send any size you want with GET and the image should be resize on-the-fly and returned
- to see the real image just use http://localhost:8000/image/img_012343.jpg
- if no size is specified in the query, then real image is sent back
- image may not exist on the server: http://localhost:8000/image/img_012343x.jpg
- a 404 error is returned as response along with 'Not Found' message

### Unit Tests

Can be run using command `npm test`
- it will run all tests found in folder /test from project root

#### Custom cache stats

- used 3rd party 'node-persist' to keep persistent cache
- link for stats: http://localhost:8000/stats

#### Aditional stats for service

- you can check this cool UI included:

http://localhost:8000/swagger-stats/metrics

- simple stats and metrics are provided for this endpoint, using 3rd-party `swagger-stats`:

http://localhost:8000/swagger-stats/stats

http://localhost:8000/swagger-stats/metrics

- those can be used with Prometheus and Grafana

#### Documentation

https://malcoded.com/posts/nodejs-image-resize-express-sharp

https://codeburst.io/javascript-unit-testing-using-mocha-and-chai-1d97d9f18e71
