const fs = require('fs');
const sharp = require('sharp');
const cache = require('./cache.js');

module.exports = function resize(path, format, width, height) {
	const readStream = fs.createReadStream(path);
	const cachedFilePath = cache.generateCachedFilePath(path, format, width, height);

	let transform = sharp();

	if (fs.existsSync(cachedFilePath)) {

	    let readCachedStream = fs.createReadStream(cachedFilePath);

	    console.log('Reading from cached file: ' + cachedFilePath);

	    cache.updateStat('hits');

	    return readCachedStream.pipe(transform);
	}

    if (format) {
        transform = transform.toFormat(format);
    }

	if (width || height) {
		transform.resize(width, height).toBuffer(
			function(err, outputBuffer) {
				if (err) {
					console.log(err)
				};
				cache.createCachedFile(cachedFilePath, outputBuffer);
			}
		);

		cache.updateStat('misses');

	    return readStream.pipe(transform);
	}

	console.log('Reading from initial file: ' + path);

    return readStream.pipe(transform);
};