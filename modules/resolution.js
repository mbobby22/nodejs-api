const parameters = require('../config/parameters.js');

exports.width = function(imageSize) {
	if (!imageSize.includes(parameters.x_char_separator)) {
		return 0;
	}

	width = imageSize.split(parameters.x_char_separator)[0];

	if (typeof width === 'undefined') {
		return 0;
	}

	return width;
}

exports.height = function(imageSize) {
	if (!imageSize.includes(parameters.x_char_separator)) {
		return 0;
	}
	
	height = imageSize.split(parameters.x_char_separator)[1];

	if (typeof height === 'undefined') {
		return 0;
	}

	return height;
}