const fs = require('fs');
const statCache = require('node-persist');
var parameters = require('../config/parameters.js');

exports.createCachedFile = function(cachedFilePath, outputBuffer) {
	let writeStream = fs.WriteStream(cachedFilePath);
	writeStream.write(outputBuffer);
	writeStream.end();

	console.log('Saved resized image to cache file: ' + cachedFilePath);
};

exports.generateCachedFilePath = function(filePath, format, width, height) {
	let path = filePath.split(format);

	return path[0] + width + parameters.x_char_separator + height + parameters.dot_char_separator + format;
};

exports.updateStat = async function (keyName) {
	await statCache.init();

	let value = await statCache.getItem(keyName);

	if (typeof value === 'undefined') {
		value = 0;
	}

	await statCache.setItem(keyName, ++value, 0);
};

exports.displayStats = async function () {
	await statCache.init();

	let hits = await statCache.getItem('hits');
    let misses = await statCache.getItem('misses');

    if (typeof hits === 'undefined') {
		hits = 0;
	}

	if (typeof misses === 'undefined') {
		misses = 0;
	}

    return 'IMAGE FILE CACHE STATS // Hits: ' + hits + '; Misses: ' + misses;
}