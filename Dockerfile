FROM node:10

WORKDIR /app

COPY . /app

RUN rm -rf /app/node_modules
RUN rm -rf /app/.node-persist
RUN npm install

EXPOSE 8000

CMD node server.js